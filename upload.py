#!/usr/bin/env python3
# For a given directory, upload all files to Mediaflux that are older than a
# given number of hours.
#
# * Make sure unimelb-mf-upload is in your path and is version > 0.5.7
# * Uses mflux.cfg in the script directory rather than ~/.Arcitecta/mflux.cfg
#   Copy mflux-sample.cfg to mflux.cfg and add your authentication details
# * Consider a secure upload token rather than storing your password, and chmod
#   the file to avoid others getting your token/password.
#   See: https://wiki-rcs.unimelb.edu.au/display/RCS/Configuration+File#ConfigurationFile-Authenticatingwithasecureidentitytoken
#
# Robert Hutton <rhutton@unimelb.edu.au> 2020-03-15

from pathlib import Path
from datetime import date, datetime, timedelta
import argparse
import subprocess
import os

def main():
    scriptdir=os.path.dirname(os.path.abspath(__file__))
    default_config=os.path.join(scriptdir,"mflux.cfg")

    parser = argparse.ArgumentParser(description='Upload files older than a hours to Mediaflux.',
            epilog=f"Example: {__file__} ~/backups /projects/proj-7890_mspf-1128.4.374 -a 6",
            formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument(dest='dir', action='store', help='directory containing files for upload')
    parser.add_argument(dest='namespace', action='store', help='destination Mediaflux namespace')
    parser.add_argument('-a', '--age', dest='age', action='store', type=int, default=6, help='ignore files newer than a hours (default 6)')
    parser.add_argument('-b', '--batch-size', dest='batchsize', action='store', type=int, default=10, help='upload files in batches of size b (default 10)')
    parser.add_argument('-c', '--cfg', dest='cfg', action='store', type=str, default=default_config, help=f'Config file to use, default: {default_config}')
    parser.add_argument('-s', '--sum', dest='sum', action='store', type=bool, default=False, help='Perform checksum check after file is uploaded (default False)')
    args = parser.parse_args()

    indir=Path(args.dir)
    namespace=args.namespace
    age=args.age
    cfg=Path(args.cfg)
    batchsize=args.batchsize
    csum=args.sum

    # find files older than a hours
    filelist=[]
    for f in indir.iterdir():
        if f.is_file():
            mtime=f.stat().st_mtime
            if mtime < datetime.timestamp(datetime.now()-timedelta(hours=age)):
                filelist.append(f)

    # batch files for upload
    # https://stackoverflow.com/questions/45105622/python-process-list-elements-in-batches
    batchedfiles=[filelist[i:i + batchsize] for i in range(0, len(filelist), batchsize)]
    for batch in batchedfiles:
        cmd=["unimelb-mf-upload", "--mf.config", cfg, "--namespace", namespace]
        if csum:
            cmd.append("--csum-check")
        subprocess.run(cmd+batch, check=True)

if __name__ == "__main__":
    main()
