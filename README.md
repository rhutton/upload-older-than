# Upload Older Than

For a given directory, upload all files to Mediaflux that are older than a given number of hours.

* Make sure unimelb-mf-upload is in your path and is version > 0.5.7
* Uses `mflux.cfg` in the script directory rather than `~/.Arcitecta/mflux.cfg`. Copy mflux-sample.cfg to mflux.cfg and add your authentication details
* Consider a secure upload token rather than storing your password, and chmod the file to avoid others getting your token/password. See: [Authenticating with a secure identity token](https://wiki-rcs.unimelb.edu.au/display/RCS/Configuration+File#ConfigurationFile-Authenticatingwithasecureidentitytoken).
